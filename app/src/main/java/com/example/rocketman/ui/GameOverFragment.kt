package com.example.rocketman.ui

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.rocketman.databinding.FragmentGameOverBinding
import androidx.navigation.fragment.findNavController
import com.example.rocketman.R

class GameOverFragment : Fragment() {
    private lateinit var binding: FragmentGameOverBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGameOverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.score.text = arguments?.getInt("score").toString()

        val music: MediaPlayer? = MediaPlayer.create(context, R.raw.game_over_music)

        music?.start()

        music?.setOnCompletionListener {
            music.start()
        }

        binding.back.setOnClickListener {
            music?.pause()
            findNavController().navigate(R.id.menuFragment)
        }
    }
}