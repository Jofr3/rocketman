package com.example.rocketman.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.rocketman.R
import com.example.rocketman.databinding.FragmentGameBinding
import com.example.rocketman.model.GameView
import com.example.rocketman.model.Player
import com.example.rocketman.mv.GameMV
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class GameFragment : Fragment() {
    private lateinit var gameView: GameView
    private lateinit var player: Player

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)

        gameView = GameView(requireContext(), size)
        return gameView
    }
}