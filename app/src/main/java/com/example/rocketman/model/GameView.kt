package com.example.rocketman.model

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.rocketman.R
import com.example.rocketman.ui.GameFragment
import com.example.rocketman.ui.GameFragmentDirections
import kotlinx.coroutines.*


@SuppressLint("ViewConstructor")
class GameView(context: Context, val size: Point) : SurfaceView(context) {
    var canvas: Canvas = Canvas()
    private val paint: Paint = Paint()

    private lateinit var player: Player
    private lateinit var background: Bitmap
    private lateinit var shotBitmap: Bitmap
    private var shots = mutableListOf<Shot>()
    private lateinit var asteroidBitmap: Bitmap
    private var asteroids = mutableListOf<Asteroid>()
    private lateinit var lifeBitmap: Bitmap
    private lateinit var playerHitBitmap: Bitmap
    private lateinit var playerHit: Bitmap
    private lateinit var explosionBitmap: Bitmap
    private var explosions = mutableListOf<Explosion>()
    private var score = 0
    private lateinit var music: MediaPlayer

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when (event.action) {
                MotionEvent.ACTION_MOVE -> {
                    player.setPos(event.x, event.y)
                }
            }
        }
        return true
    }

    init {
        startGame()
    }

    private var delay = 400L
    private fun startGame() {
        createElements()

        CoroutineScope(Dispatchers.Main).launch {
            launch {
                while (player.lives > 0) {
                    update()
                    draw()
                    delay(10)
                }


                music.pause()
                val action = GameFragmentDirections.actionGameFragmentToGameOverFragment(score)
                findNavController().navigate(action)

            }

            launch {
                while (player.lives > 0) {
                    delay(delay)
                    if (delay == 500L) {
                        delay = 400L
                    }
                    shoot()
                }
            }

            launch {
                while (player.lives > 0) {
                    var spawnDelay = 3000L
                    spawnDelay -= score
                    if(spawnDelay < 1000){
                        spawnDelay = 1000
                    }
                    delay(spawnDelay)
                    newAsteroid()
                }
            }
        }
    }

    private fun createElements() {
        music = MediaPlayer.create(context, R.raw.game_music)

        music.start()

        music.setOnCompletionListener {
            music.start()
        }


        val backgroundRaw: Bitmap =
            BitmapFactory.decodeResource(context?.resources, R.drawable.background)
        background = Bitmap.createScaledBitmap(backgroundRaw, size.x, size.y, false)
        shotBitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.projectile)
        asteroidBitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.asteroid)
        shotBitmap = Bitmap.createScaledBitmap(
            shotBitmap,
            (((size.y / 20) * shotBitmap.width) / shotBitmap.height),
            (size.y / 20),
            false
        )
        lifeBitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.life)
        playerHitBitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.shiphit)
        playerHit = Bitmap.createScaledBitmap(
            playerHitBitmap, ((size.y / 10 * playerHitBitmap.width) / playerHitBitmap.height),
            size.y / 10,
            false
        )
        explosionBitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.explosion)

        val ship: Bitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.ship)
        player = Player(ship, size.x, size.y, 3, playerHit)
    }

    private fun draw() {
        if (holder.surface.isValid) {
            val canvas = holder.lockCanvas()

            canvas.drawBitmap(background, 0f, 0f, paint)
            for (shot in shots) {
                canvas.drawBitmap(shot.img, (shot.x - shot.img.width / 2), shot.y, paint)
            }
            for (asteroid in asteroids) {
                canvas.drawBitmap(
                    asteroid.img,
                    (asteroid.x - player.img.width / 2),
                    asteroid.y,
                    paint
                )
            }
            canvas.drawBitmap(player.img, (player.x - player.img.width / 2), player.y, paint)
            for (i in 0 until player.lives) {
                canvas.drawBitmap(lifeBitmap, i * 90f, size.y - 100f, paint)
            }
            for (explosion in explosions) {
                canvas.drawBitmap(
                    explosion.img,
                    explosion.x,
                    explosion.y,
                    paint
                )
            }

            val plain = Typeface.createFromAsset(context.assets, "pixel_emulator.otf")
            paint.typeface = plain
            paint.color = Color.WHITE
            paint.textSize = 70f
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawText("$score", (size.x - paint.descent()), 75f, paint)



            holder.unlockCanvasAndPost(canvas)
        }
    }

    private fun update() {
        player.move()

        for (shot in shots) {
            shot.move()

            val shotPos = RectF(shot.x, shot.y, shot.x + shot.img.width, shot.y + shot.img.height)
            for (asteroid in asteroids) {
                val asteroidPos = RectF(
                    asteroid.x,
                    asteroid.y,
                    asteroid.x + asteroid.img.width,
                    asteroid.y + asteroid.img.height
                )
                if (RectF.intersects(shotPos, asteroidPos)) {
                    shots.remove(shot)
                    asteroid.hp--
                    if (asteroid.hp <= 0) {
                        asteroidExplode(asteroid)
                        asteroids.remove(asteroid)
                        delay = 500L
                        val points = asteroid.startHp * 25
                        score += points.toInt()
                    }
                    return
                }
            }

            if (shot.y > size.y) {
                shots.remove(shot)
            }
        }

        for (asteroid in asteroids) {

            if (asteroid.y < 0) {
                asteroids.remove(asteroid)
            } else  {
                val asteroidPos = RectF( asteroid.x, asteroid.y, asteroid.x + asteroid.img.width, asteroid.y + asteroid.img.height )
                val playerPos = RectF(player.x, player.y, player.x + player.img.width, player.y + player.img.height)

                if (RectF.intersects(asteroidPos, playerPos)) {
                    asteroidExplode(asteroid)
                    asteroids.remove(asteroid)
                    player.lives--
                    player.hit()
                    return
                }
            }

            asteroid.move()
        }
    }

    private fun shoot() {
        CoroutineScope(Dispatchers.Main).launch {
            val shot = Shot(shotBitmap, player.x, player.y, size.y)
            shots.add(shot)
        }
    }

    private fun newAsteroid() {
        val asteroid = Asteroid(asteroidBitmap, size.x, size.y)
        asteroids.add(asteroid)
    }

    private fun asteroidExplode(asteroid: Asteroid) {
        val explosionImg = Bitmap.createScaledBitmap(
            explosionBitmap,
            asteroid.img.width, asteroid.img.height,
            false
        )

        val explosion = Explosion(explosionImg, asteroid.x - 90, asteroid.y)

        explosions.add(explosion)

        CoroutineScope(Dispatchers.Main).launch {
            delay(800)
            explosions.remove(explosion)
        }
    }
}




