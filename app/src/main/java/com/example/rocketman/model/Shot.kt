package com.example.rocketman.model

import android.graphics.Bitmap

class Shot(bitmap: Bitmap, playerX: Float, playerY: Float, screenHeight: Int) {
    var img: Bitmap = bitmap
    private val height = screenHeight / 20f
    var x = playerX
    var y = playerY
    var vel = 20f

    fun move() {
        y -= vel
    }
}