package com.example.rocketman.model

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.rocketman.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Player(bitmap: Bitmap, screenWidth: Int, screenHeight: Int, var lives: Int,
             private val playerHit: Bitmap
) {
    var img: Bitmap
    private val height = screenHeight / 10f
    var x = screenWidth / 2f
    var y = screenHeight * 0.8f
    var vel = 10f

    init {
        img = Bitmap.createScaledBitmap(
            bitmap,
            ((height.toInt() * bitmap.width) / bitmap.height),
            height.toInt(),
            false
        )
    }

    private var rawPosX = x
    private var rawPosY = y

    fun setPos(mouseX: Float, mouseY: Float) {
        rawPosX = mouseX
        rawPosY = mouseY
    }

    fun move() {
        val endPosX = x - rawPosX
        val endPosY = y - (rawPosY - height / 2f)

        x -= endPosX / vel
        y -= endPosY / vel
    }

    fun hit() {
        CoroutineScope(Dispatchers.Main).launch {
            val oldImg = img
            img = playerHit
            delay(800)
            img = oldImg
        }
    }
}
