package com.example.rocketman.model

import android.graphics.Bitmap
import android.graphics.Matrix
import androidx.core.graphics.get
import kotlinx.coroutines.CoroutineScope
import kotlin.random.Random

class Asteroid(bitmap: Bitmap, screenWidth: Int, screenHeight: Int) {
    var img: Bitmap
    private var height = 0
    var x = 0f
    var y = 0f
    var vel = 5f
    var hp = 0f
    var startHp = 0f

    init {
        height = Random.nextInt(100, 500)
        vel = (900 - height) / 100f
        hp = height / 100f
        startHp = hp
        img = Bitmap.createScaledBitmap(
            bitmap,
            ((height * bitmap.width) / bitmap.height),
            height,
            false
        )

        x = Random.nextInt(0, screenWidth).toFloat()
        y = Random.nextInt(0, 1).toFloat()
    }

    fun move() {
        y += vel
    }
}